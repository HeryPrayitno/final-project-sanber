<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('', function () {
    return view('welcome');
});

Route::get('/products', function () {
    return view('landing.products');
});

Route::get('/cart', function () {
    return view('landing.cart');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('pesan/{id}', [App\Http\Controllers\PesanController::class, 'index']);

Route::get('pesan/{id}', [App\Http\Controllers\PesanController::class, 'pesan']);
 
//CREATE
//Form Tambah Kategori
Route::get('/kategori/create', [App\Http\Controllers\KategoriController::class, 'create']);

//Buat Kirim data ke database
Route::post('/kategori', [App\Http\Controllers\KategoriController::class, 'store']);

//READ
//Buat Read Data
Route::get('/kategori', [App\Http\Controllers\KategoriController::class, 'index']);

// Detail Kategori
Route::get('/kategori/{kategori_id}', [App\Http\Controllers\KategoriController::class, 'show']);

//UPDATE
Route::get('/kategori/{kategori_id}/edit', [App\Http\Controllers\KategoriController::class, 'edit']);

//Update Data ke Database berdasarkan ID
Route::put('/kategori/{kategori_id}', [App\Http\Controllers\KategoriController::class, 'update']);


// //DELETE

Route::delete('/kategori/{kategori_id}', [App\Http\Controllers\KategoriController::class, 'destroy']);

// CRUD Barang

//CREATE
//Form Tambah Kategori
Route::get('/barang/create', [App\Http\Controllers\BarangController::class, 'create']);

//Buat Kirim data ke database
Route::post('/barang', [App\Http\Controllers\BarangController::class, 'store2']);

//READ
//Buat Read Data
Route::get('/barang', [App\Http\Controllers\BarangController::class, 'index']);

// Detail Kategori
Route::get('/barang/{barang_id}', [App\Http\Controllers\BarangController::class, 'show']);

//UPDATE
Route::get('/barang/{barang_id}/edit', [App\Http\Controllers\BarangController::class, 'edit']);

//Update Data ke Database berdasarkan ID
Route::put('/barang/{barang_id}', [App\Http\Controllers\BarangController::class, 'update']);


// // //DELETE

Route::delete('/barang/{kategori_id}', [App\Http\Controllers\BarangController::class, 'destroy']);


