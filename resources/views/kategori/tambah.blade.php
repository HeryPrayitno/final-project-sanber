@extends('layouts.app')


@section('content')
<form action="/kategori" method="POST">
    @csrf
      <div class="form-group">
        <label>Kategori</label>
        <input type="text" name="kategoris" class="form-control">
      </div>
      @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection