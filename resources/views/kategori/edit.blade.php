@extends('layouts.app')

@section('title')
Halaman Update Cast    
@endsection

@section('sub-title')
Create Data Cast
@endsection

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Kategori</label>
      <input type="text" name="kategori" value="{{$kategori->kategori}}" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>  

@endsection