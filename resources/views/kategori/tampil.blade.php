@extends('layouts.app')


@section('sub-title')
Data Category Penjualan
@endsection

@section('content')


<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Kategori</th>
        <th scope="col">Action</th>
        <a href="/kategori/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
      </tr>
    </thead>
    <tbody>
      @forelse ($kategori as $key => $value)
      <tr>
        <td>{{$key + 1}}</td>
        <td>{{$value->kategori}}</td>
        <td>
            <form action="/kategori/{{$value->id}}" method="POST">
                @csrf
                <a href="/kategori/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                
            </form>
        </td>
    </tr>
    @empty
    <tr>
        <td>Tidak Ada data</td>
    </tr>
    @endforelse
    </tbody>
  </table>
@endsection