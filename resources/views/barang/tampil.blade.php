@extends('layouts.app')


@section('sub-title')
Data Category Penjualan
@endsection

@section('content')


<table class="table table-dark">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Harga</th>
        <th scope="col">Stok</th>
        <th scope="col">Keterangan</th>
        <th scope="col">Action</th>
        <a href="/barang/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
      </tr>
    </thead>
    <tbody>
      @forelse ($barang as $key => $value)
      <tr>
        <td>{{$key + 1}}</td>
        <td>{{$value->nama}}</td>
        <td>{{$value->harga}}</td>
        <td>{{$value->stok}}</td>
        <td>{{$value->keterangan}}</td>
        <td>
            <form action="/barang/{{$value->id}}" method="POST">
                @csrf
                <a href="/barang/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                
            </form>
        </td>
    </tr>
    @empty
    <tr>
        <td>Tidak Ada data</td>
    </tr>
    @endforelse
    </tbody>
  </table>
@endsection