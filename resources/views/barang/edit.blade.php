@extends('layouts.app')

@section('content')
<form action="/barang/{{$barang->id}}" method="POST">
    @csrf
    @method('PUT')
      <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$barang->nama}}" class="form-control">
      </div>
      @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <label>Harga</label>
      <input type="text" name="harga" value="{{$barang->harga}}" class="form-control">
    </div>
    @error('harga')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>Stok</label>
    <input type="text" name="stok" value="{{$barang->stok}}" class="form-control">
  </div>
  @error('stok')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group form-check">
    <label>Keterangan</label>
    <textarea name="keterangan" class="form-control" cols="30" rows="10" >{{$barang->keterangan}}</textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label>ID Kategori</label>
    <input type="text" name="kategoris_id" class="form-control">
  </div>
  @error('kategoris_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection