<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function create()
    {
        return view('barang.tambah');
    }

    public function store2(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'keterangan' => 'required',
            'kategoris_id' => 'required'
        ]);

    DB::table('barangs')->insert([
        'nama' => $request['nama'],
        'harga' => $request['harga'],
        'stok' => $request['stok'],
        'keterangan' => $request['keterangan'],
        'kategoris_id' => $request['kategoris_id'],
    ]);

    return redirect('/barang');
    }
    public function index()
    {
        $barang = DB::table('barangs')->get();
        // dd($cast);

        return view('barang.tampil', ['barang' => $barang]);
    }
    public function show($id)
    {
        $barang = DB::table('barangs')->where('id', $id)->first();

        return view('barang.detail', ['barang' => $barang]);
    }
    public function edit($id)
    {
        $barang = DB::table('barangs')->where('id', $id)->first();

        return view('barang.edit', ['barang' => $barang]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'keterangan' => 'required',
            'kategoris_id' => 'required'
        ]);

        DB::table('barangs')
            ->where('id', $id)
            ->update(
                [   
                    'nama' => $request['nama'],
                    'harga' => $request['harga'],
                    'stok' => $request['stok'],
                    'keterangan' => $request['keterangan'],
                    'kategoris_id' => $request['kategoris_id'],
                ],
            );

            return redirect('/barang');
    }
    public function destroy($id){
        
        DB::table('barangs')->where('id', $id)->delete();

        return redirect('/barang');
    }
}
