<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'kategoris' => 'required'
        ]);

    DB::table('kategoris')->insert([
        'kategori' => $request['kategoris']
    ]);

    return redirect('/kategori');
    }

    public function index()
    {
        $kategori = DB::table('kategoris')->get();
        // dd($cast);

        return view('kategori.tampil', ['kategori' => $kategori]);
    }
    public function show($id)
    {
        $kategori = DB::table('kategoris')->where('id', $id)->first();

        return view('kategori.detail', ['kategori' => $kategori]);
    }

    public function edit($id)
    {
        $kategori = DB::table('kategoris')->where('id', $id)->first();

        return view('kategori.edit', ['kategori' => $kategori]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
        ]);

        DB::table('kategoris')
            ->where('id', $id)
            ->update(
                [   
                    'kategori' => $request->kategori,
                ],
            );

            return redirect('/kategori');
    }
    

    public function destroy($id){
        
        DB::table('barangs')->where('id', $id)->delete();

        return redirect('/barangs');
    }
    
}

