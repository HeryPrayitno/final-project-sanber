<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan_Detail extends Model
{
    public function barang()
	{
	      return $this->belongsTo('App\Barang','barangs_id', 'id');
	}

	public function pesanan()
	{
	      return $this->belongsTo('App\Pesanan','pesanans_id', 'id');
	}
}
